uint64_t reg_[64];
uint64_t reg_acc;
uint64_t reg_pc;
uint8_t reg_flag;
uint8_t * tape;

void accumulate(uint64_t input) {
	if (reg_flag < 0x10) {
		switch (reg_flag) {	
			case 0 : reg_acc += input; break;
			case 1 : reg_acc -= input; break;
			case 2 : reg_acc &= input; break;
			case 3 : reg_acc |= input; break;
			case 4 : reg_acc = ~reg_acc | ~input; break;
			case 5 : reg_acc = ~reg_acc & ~input; break;
			case 6 : reg_acc ^= input; break;
			case 7 : reg_acc = (reg_acc & input) | (~reg_acc & ~input); break;
			case 8 : reg_acc <<= input; break;
			case 9 : reg_acc = (reg_acc << (input % 8)) | (0xFFFFFFFFFFFFFFFF >> (8 - (input % 8))); break;
			case 0xa : reg_acc = (reg_acc << (input % 8)) | (((1 & reg_acc) * 0xFFFFFFFFFFFFFFFF) >> (8 - (input % 8))); break;
			case 0xb : reg_acc >>= input; break;
			case 0xc : reg_acc = (reg_acc << (8 - (input % 8))) | (0xffffffffffffffff >> (input % 8)); break;
			case 0xd : reg_acc = (int64_t) reg_acc >> input; break;
			case 0xe : reg_acc = (reg_acc << (input % 8)) | (reg_acc >> (8 - (input % 8))); break;
			case 0xf : reg_acc = (reg_acc << (8 - (input % 8))) | (reg_acc >> (input % 8)); break;
			default : puts("cosmic rays detected, please return to your spacecraft."); abort();
}	}	}

void add8 (uint8_t * x, uint8_t * y, uint8_t * r) {
	*r = *x + *y;
	accumulate(*x + *y);
	}

main(int argc,char *argv[]) {
	if (argc != 3) {
		printf("Usage: %s bytecode_file");
		}
	FILE *fp = fopen(argv[1],"r");
	tape = sbrk(0);
	fseek(fp,0,SEEK_END);
	sbrk(ftell(fp) + 1);
	fseek(fp,0,SEEK_SET);
	{
	char ch;
	for (unsigned long n;;n++) {
		ch = fgetc(fp);
		if (ch == EOF) {
			break;
			}
		tape[n] = ch;
	}	}
	fclose(fp);
	for (;;) {
		opcode = tape[reg_pc++];
		subop = tape[reg_pc++]
		switch (opcode & 0x7f) {
			case op_noop : break;
			case op_add : 
			case op_sub : 
			case op_mul : 
			case op_div : 
			case op_mod : 
			case op_pow : 
			case op_and : 
			case op_or : 
			case op_nand : 
			case op_nor : 
			case op_xor : 
			case op_xnor : 
			case op_land : 
			case op_lor : 
			case op_lnand : 
			case op_lnor : 
			case op_lxor : 
			case op_iff : 
			case op_lonehot : 
			case op_levenp : 
			case op_loddp : 
			case op_lt : 
			case op_ge : 
			case op_le : 
			case op_ge : 
			case op_eq : 
			case op_ne : 
			case op_min : 
			case op_max : 
			case op_cmp : 
			case op_slzf : 
			case op_slof : 
			case op_slsf : 
			case op_srzf : 
			case op_srof : 
			case op_srsf : 
			case op_rotl : 
			case op_rotr : 
			case op_inc : 
			case op_dec : 
			case op_mand : 
			case op_mor : 
			case op_mnand : 
			case op_mnor : 
			case op_mxor : 
			case op_mxnor : 
			case op_mslzf : 
			case op_mslof : 
			case op_mslsf : 
			case op_msrzf : 
			case op_msrof : 
			case op_msrsf : 
			case op_mrotl : 
			case op_mrotr : 
			case op_bitlength : 
			case op_signlength : 
			case op_bitcount : 
			case op_sgn : 
			case op_not : 
			case op_uand : 
			case op_bool : 
			case op_unand : 
			case op_unor : 
			case op_uxnor : 
			case op_onehot : 
			case op_evenp : 
			case op_oddp : 
			case op_invert : 
			case op_abs : 
			case op_if : 
			case op_unless : 
			case op_move : 
			case op_memcpy : 
			case op_memmove : 
			case op_brk : 
			case op_sbrk : 
			case op_end : 
			default : exit(1);
			}
