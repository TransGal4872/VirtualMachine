the filename and the amount of memory to sbrk are given as command line arguments

numbers may be provided in
hexadecimal : $
octal : @
binary : %
or decimal : #

an instruction with a type will change the default storage size until ;
rest of the line, to change this manually, use a number in <>
a number may be prefixed with - or ~ to perform two's complement or
bitwise negation.
an address is written in []
opcodes can be prefixed with - to perform the signed version of the op

provided 2 argument functions:
	>register >indirect >direct
	<register <indirect <direct <literal
	.8 .16 .32 .64
add sub mul div mod pow and or
nand nor xor xnor land lor lnand lnor
lxor iff lonehot levenp loddp lt gt le
ge eq ne min max cmp slzf slof
slsf srzf srof srsf rotl rotr

increment and decrement:
	&register &direct &indirect
	.8 .16 .32 .64
inc dec mand mor
mnand mnor mxor mxnor
mslzf mslof mslsf msrzf
msrof msrsf mrotl mrotr
(m for mutate)

provided single argument functions:
	>register >indirect >direct
	<register <indirect <direct <literal
	.8 .16 .32 .64
bitlength signlength bitcount sgn
not uand bool unand 
unor uxnor onehot evenp
oddp invert abs
(u for unary)

special operations:
noop	// do nothing. for compatibility with GCL
if (direct|indirect|register) literal	//if false, skip literal bytes
unless (direct|indirect|register) literal	//if true, skip literal bytes
end	//exit(0)

print source(any) length(any)
print length from source to stdout

coredump
dump the contents of all registers in an implementation defined order

memory commands:
	>indirect >direct >register
	<indirect <direct <register <literal
move	.8 .16 .32 .64

	>direct >indirect >register
	<direct <indirect <register
	.8 .16 .32 .64
memcpy memmove

(reading more elements than possible from the register's vector should
raise sigsegv or equivilent)


(Note: for all move commands larger than one byte in memory or smaller
than the full register size, the reference implementation uses memcpy()
for moves between memory and registers and memmove for moves between
memory and memory, and uses a checked memcpy for moves between register
and register. single byte memory-memory transfers and transfers between
equal sized whole registers use assignment.)

	<direct <indirect <literal <register
sbrk 	//change the size of the Tape, always 32 bit

Accumulator modes:
accinc accdec accand accor
accnand accnor accxor accxnor
accrotl accrotr noacc

Registers:
64bit×16
Acc 64bit
PC 64bit
AccMode 8bit
a symbol tape should be provided, the reference implementation casts sbrk(0) to unsigned char * to create one.

types:
.8 .16 .32 .64

sooooooo ccbbaatt
o = opcode
s = signedness
t = type
a b c = address mode of arguments
